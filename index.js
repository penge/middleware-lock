const express = require('express')
const app = express()
const middlewares = require('./middlewares')
const actions = require('./actions')


// Sets lock before every route, which:
// - contains :user param
// - is POST action
app.param('user', middlewares.setLock(['POST']));



// Middleware that will decide if operator is eligible
// to perfom action based on the "lock"
app.param('user', middlewares.canAct);



// Using actions
app.post('/issues/:user', actions.createIssue)



app.listen(5000, () => {
  console.log('Running on port:', 5000)
})
