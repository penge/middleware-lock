const util = require('util')


const inspect = object => {
  return util.inspect(object, {
    colors: true
  })
}


module.exports = inspect;
