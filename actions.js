const inspect = require('./inspect')


const createIssue = function (req, res) {
  console.log('[ACT] createIssue --- Can act based on lock:\n', inspect(req.lock), '\n');
  console.log('[ACT] createIssue --- Creating new issue');

  const newIssue = {
    name: 'New issue name.', // read from body
    lock: req.lock  // (will not be part of response)
  };

  console.log('[ACT] createIssue --- Returning new issue response');
  res.json(newIssue);
}


module.exports = {
  createIssue
}
