const inspect = require('./inspect')


// TODO: Decode current operator from JWT
const decodeOperator = token => {
  return {
    name: 'Andres',
    email: 'andres@operator.com', // CHANGE TO TEST "canAct"
    isAdmin: true // CHANGE TO TEST "canAct"
  }
}


const userFactory = email => {
  return {
    email
  }
}


// TODO: Read lock from DynamoDB
const readLock = userEmail => {
  const lock = {
    email: userEmail,
    operator: 'oliver@operator.com' // CHANGE TO TEST "canAct"
  }

  // Proxy
  return {
    user: { email: lock.email },
    operator: { email: lock.operator }
  }
}


const setOperator = function (req, res, next) {
  const token = 'getTokenFromHeaders';
  const operator = decodeOperator(token);
  console.log('[MID] setOperator --- Setting operator to:\n', inspect(operator), '\n');
  req.operator = operator; // THIS IS WHAT WE NEED
  next();
}


const setUser = function (req, res, next) {
  const user = userFactory(req.params.user);
  console.log('[MID] setUser --- Setting user to:\n', inspect(user), '\n');
  req.user = user; // THIS IS WHAT WE NEED
  next();
}


const setLock = (methods) => {
  return function (req, res, next) {
    if (methods.indexOf(req.method) === -1) {
      console.lock('[MID] setLock --- NOT CALLED!');
      return next();
    }

    setOperator(req, res, () => {});
    setUser(req, res, () => {});

    const lock = readLock(req.user.email);

    console.log('[MID] setLock --- Intercepting', req.method, req.url);
    console.log('[MID] setLock --- Setting lock to:\n', inspect(lock), '\n');

    req.lock = lock; // THIS IS WHAT WE NEED
    next();
  }
}


const canAct = function (req, res, next) {
  const lock = req.lock;
  const operator = req.operator;

  // There is no lock
  if (!lock) {
    console.log('[MID] canAct --- There is no lock');
    return next();
  }

  const isLockMadeByMe = lock.operator.email === operator.email;
  const amIAdmin = operator.isAdmin === true;
  const canAct = isLockMadeByMe || amIAdmin;

  if (canAct) {
    console.log('[MID] canAct --- TRUE');
    console.log('[MID] canAct --- isLockMadeByMe:', isLockMadeByMe);
    console.log('[MID] canAct --- amIAdmin:', amIAdmin);
    return next();
  }

  console.log('[MID] canAct --- FALSE');
  res.status(401).json({
    status: 'Unauthorized'
  });
}


module.exports = {
  setLock,
  canAct
}
